<?php
$capabilities = array(

	'block/user_courses:myaddinstance' => array(
		'captype' => 'write',
		'contextlevel' => CONTEXT_SYSTEM,
		'archetypes' => array(
			'user' => CAP_ALLOW
		),
		'clonepermissionsform' => 'moodle/my:manageblocks'
	),

	'block/user_courses:addinstance' => array(
		'riskbitmask' => RISK_SPAM|RISK_XSS,

		'captype' => 'write',
		'contextlevel' => CONTEXT_BLOCK,
		'archetypes' => array(
			'editingteacher' => CAP_ALLOW,
			'manager' => CAP_ALLOW
		),
		'clonepermissionsform' => 'moodle/site:manageblocks'
	),
);