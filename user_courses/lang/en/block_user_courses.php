<?php
$string['pluginname'] = 'User courses';
$string['user_courses'] = 'User Courses';
$string['user_courses:addinstance'] = 'Add a new User courses block';
$string['user_courses_title'] = 'User Courses';
$string['user_courses:myaddinstance'] = 'Add a new User courses block to My Moodle page';
$string['defaulttitle'] = 'Add a new User courses block to My Moodle page';
$string['token'] = 'Web Service user token';