<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package     block_user_courses_edit_form
 * @author      2015 Nikos Papathanasiou <p2nikos@yahoo.gr>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright   (C) 1999 onwards Martin Dougiamas  http://dougiamas.com
 *
 * The user courses block instance settings
 */

class block_user_courses_edit_form extends block_edit_form {

	protected function specific_definition($mform) {
		// simple block instance settings (each user can modify for his own page)
		$mform->addElement('header', 'configheader', get_string('blocksettings', 'block'));

		$mform->addElement('text', 'config_title', get_string('user_courses_title', 'block_user_courses'));
		$mform->setDefault('config_title', 'default value');
		$mform->setType('config_title', PARAM_TEXT);
	}
}