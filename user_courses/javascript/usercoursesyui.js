/*
    Global variables to simplify paging (starting entry and number of entries per page)
*/
var start_record = 0;
var page_records = 2;

YUI().use('node-event-delegate', 'event-hover', 
    'transition', 'overlay', 'node',
    function(Y) {
    // prepare all the "tooltip" popups and hide them
    var uca = Y.all('.user_courses_activities_div')
        .setStyle('position', 'absolute')
        .setStyle('margin-top', '-20px')
        .setStyle('margin-left', '20px')
        .transition('fadeOut');

    var cnt_uca = Y.all('.user_course').size();

    var total_records = Y.all('.user_course').size();

    // call the initial pagination function
    paginate(start_record, page_records, total_records, 0);

    // call the next - previous function on the responsive buttons click
    var direction = 0;
    Y.one('#previous_page').delegate('click', function(e) {
        direction = 1;
        paginate(start_record, page_records, total_records, direction);
    }, 'input');
    Y.one('#next_page').delegate('click', function(e) {
        direction = 2;
        paginate(start_record, page_records, total_records, direction);
    }, 'input');

    var pg = Y.one('.page_navigation');

    // delegate hover over a course name so we can show it's own tip 
    Y.delegate('hover', 
    function(e) {
        var we = e.currentTarget.one('.user_courses_activities_div');
        we.transition('fadeIn');
    },
    '.user_course',
    function(e) {
        var we = e.currentTarget.one('.user_courses_activities_div');
        we.transition('fadeOut');
    })
    
    // simple pagination function (should be improved and not using global vars)
    function paginate(start, limit, total, page_dir) {
        var uc = Y.all('.user_course');
        if (page_dir==2 && start + limit <= total) {
            start = start + limit;
        }
        else if (page_dir==1 && start-limit >= 0) {
            start = start - limit;
        }
        
        // check each element (course name) to show and hide the appropriate 
        Y.each(uc, function(v, k) {
            //alert(start + ' ' + k + ' ' + limit + ' ' + (limit+start));
            if (k>=start && k<limit+start) {
                v.show();
            }
            else {
                v.hide();
            }
        });
    };
});