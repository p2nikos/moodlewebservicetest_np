<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package     sql_get_user_courses
 * @author      2015 Nikos Papathanasiou <p2nikos@yahoo.gr>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright   (C) 1999 onwards Martin Dougiamas  http://dougiamas.com
 *
 * The SQL manipulation class for User courses block
 */

class sql_get_user_courses {
    public function get_user_courses($course_ids) {
        global $DB;

        // get the user courses and the included activities
        $query= 'SELECT c_modules.id AS id, c_modules.course AS course_id, modules.name AS activity_name
                FROM
                {course_modules} c_modules
                INNER JOIN {modules} modules ON c_modules.module = modules.id
                WHERE
                EXISTS (select 1 from {course_modules} 
                WHERE {course_modules}.course IN (:courseids))
                AND c_modules.visible = :visible 
                ;';
        $params['courseids'] = $course_ids;
        $params['visible'] = 1;

        // preferred to use client side paging through YUI to present a smoother interface without unnececary posts
        // anyway all that it is loaded in memory is course and activity names
        // in a really MUCH larger site server side pagination may be preferrable
        $limitfrom = 0;
        $limitnum = 0;

        $allentries = $DB->get_records_sql($query, $params, 0, $limitnum);

        return $allentries;
    }
}

?>