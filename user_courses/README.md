User courses block
========================

Displays a block with the current user's courses and the number of activities within each course.
Uses a Web service to get this information

(tested with Moodle 2.9)
========================
To install this block:

1) Copy the block_user_courses inside the moodle/blocks directory
2) Enable Web Services
3) Create a user with REST capabilities
4) Create a token for this user for the get_user_courses_service
5) Copy this token to the block's settings page
6) Any user (with the relative rights) can add this block to his front page.


-------------------------------------------
Points for future improvement
- Pagination buttons are always shown (Previous button in the first page etc.)
- Pagination limit is hardcoded within Javascript (2 entries). This must change to configuration variable

-------------------------------------------
Nikos Papathanasiou
p2nikos@yahoo.gr