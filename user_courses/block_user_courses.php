<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package     block_user_courses
 * @author      2015 Nikos Papathanasiou <p2nikos@yahoo.gr>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright   (C) 1999 onwards Martin Dougiamas  http://dougiamas.com
 *
 * The user courses block
 */

class block_user_courses extends block_base {
	public function init() {
		$this->title = get_string('user_courses', 'block_user_courses');
	}

// To use configuration values outside init (ie. set the title)
/*	public function specialization() {
		if (isset($this->config)) {
			if (empty($this->config->title)) {
				$this->title = get_string('defaulttitle', 'block_user_courses');
			} else {
				$this->title = $this->config->title;
			}   
		}
	}*/

	public function get_content() {
		if ($this->content !== null) {
			return $this->content;
		}

		$this->content = new stdClass;
		$this->content->text = '';

        // prepare the two navigation buttons in the block footer
        $footer = html_writer::start_tag('div', array('class' => 'page_navigation'));
        $footer .= html_writer::tag('input', '', array('id' => 'previous_page', 
                                                       'type' => 'button',
                                                       'value' => 'Previous'));

        $footer .= html_writer::tag('input', '', array('id' => 'next_page', 
                                                       'type' => 'button',
                                                       'value' => 'Next'));
        $footer .= html_writer::end_tag('div');
		$this->content->footer = $footer;

		global $CFG;
		global $PAGE;

        // load the YUI javascript for tips and navigation
		$PAGE->requires->js('/blocks/user_courses/javascript/usercoursesyui.js');

		/// SETUP - these values must be set in the block installation from the administrator
		//$token = '941bf0cf00c78632d566b00bc3729dfc';

        // get the necessary configuration values from the installation
        $token = $CFG->user_courses_token;
		$domainname = $CFG->wwwroot;

		$functionname = 'get_user_courses';   // the name of our new service
		// REST RETURNED VALUES FORMAT
		$restformat = 'json'; // using json format for the REST call

		/// PARAMETERS 
		header('Content-Type: text/plain');

		global $USER;

		$data = array("userid" => $USER->id);
		$data_string = json_encode($data);

        // We are using curl which is already available in Moodle 2.9 (and previous versions)
		$curl_init_string = $domainname . '/webservice/rest/server.php'
		. '?wstoken=' . $token 
		. '&wsfunction=' . $functionname 
		. '&moodlewsrestformat=' . $restformat;

		$curl = new curl();
		$resp = $curl->post($curl_init_string, $data);
		$response_decoded = json_decode($resp);

        // deconding the web service result and get the user course names and their activities
		$course_names = array();
		foreach ($response_decoded as $course) {
			$activities_div = '';
			if (count($course->activities)>0) {
				$activities_div = '<div class="user_courses_activities_div">';
				foreach ($course->activities as $activity_name) {
					$activities_div .= $activity_name . '<br/>';
				}				
				$activities_div .= '</div>';
			}

			$course_names[] = 
                    '<tr class="user_course"><td>'
							.$course->course_name
							.'</td><td>'
							.count($course->activities)
							.$activities_div
							.'</td></tr>';
		}
        // combine the course names into a space separated string
		$course_names_string = implode(" ", $course_names);

		//$this->content->text = html_write::tag('table', )
		$this->content->text = '<table id="user_courses_table">'
			.$course_names_string.'</table>';
//-------

		return $this->content;
	}

    /**
     * Allow the block to have a configuration page
     *
     * @return boolean
     */
    public function has_config() {
        return true;
    }

}