<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package     block_user_courses
 * @author      2015 Nikos Papathanasiou <p2nikos@yahoo.gr>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright   (C) 1999 onwards Martin Dougiamas  http://dougiamas.com
 *
 * The Web service providing data for the user courses block
 */

require_once($CFG->libdir."/externallib.php");

class local_user_courses_external extends external_api {

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function get_user_courses_parameters() {
        return new external_function_parameters(
            array('userid' => new external_value(PARAM_TEXT, 
                'The current user\'s id "', VALUE_DEFAULT, '0'))
            );

    }

    public static function get_user_courses($userid = '0') {
        require_once("sqlgetusercourses.php");

        global $USER;

        //Parameter validation
        $params = self::validate_parameters(self::get_user_courses_parameters(),
            array('userid' => $userid));

        // get the current context to execute the web service 
        $context = get_context_instance(CONTEXT_USER, $USER->id);
        self::validate_context($context);

        // get the courses that the user is enrolled 
        $courses = enrol_get_users_courses($userid);

        // get the user courses ids
        $course_ids = array_map(function($o) { return $o->id; }, $courses);
        // create a string parameter with these ids for the sql query to get the activities
        $course_ids_string = implode(",", $course_ids);

        // create an instance of the sql handling class
        $sql_user_courses = new sql_get_user_courses();
        $user_courses_activities = $sql_user_courses->get_user_courses($course_ids_string);

        // parsing the sql results into objects that can be returned by REST
        $courseret = array();
        foreach($courses as $course) {
            // each course contains an id, a name and 0 or more activities
            $course_info = array();
            $course_info['id'] = $course->id;
            $course_info['course_name'] = $course->fullname;

            $course_activity = array();
            foreach ($user_courses_activities as $activities) {
                if ($activities->course_id == $course->id) {
                    $course_activity[] = $activities->activity_name;
                }
            }
            $course_info['activities'] = $course_activity;
            $courseret[] = $course_info;
        }

        return $courseret;
    }

    /**
     * Returns description of method results
     * @return external_function_results
     */
    public static function get_user_courses_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id'                => new external_value(PARAM_RAW, 'id'),
                    'course_name'       => new external_value(PARAM_RAW, 'course_name'),
                    'activities'        => new external_multiple_structure (
                        new external_value(PARAM_RAW, 'activities')
                        )
                    )
                )
            );

    }

}